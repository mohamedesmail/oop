package eg.edu.alexu.csd.filestructure.sort.cs53;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import eg.edu.alexu.csd.filestructure.sort.IHeap;
import eg.edu.alexu.csd.filestructure.sort.INode;

public class MyHeap<T extends Comparable<T>> implements IHeap<T> {
	int size = 0;
	ArrayList<MyNode<T>> list = new ArrayList<>();
	

	@Override
	public INode<T> getRoot() {
		if (size != 0)
			return list.get(0);
		return null;
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return size;
	}

	@Override
	public void heapify(INode<T> node) {
		heapify(((MyNode<T>)node).index);
		
	}

	@Override
	public T extract() {
		if (size == 0)
			throw null;
		
		MyNode<T> temp = list.get(0);
		list.set(0, list.get(size - 1));
		list.get(0).index = 1;
		list.remove(size - 1);
		size--;
		if (size != 0)
			heapify(1);
		
		return temp.getValue();
	}

	@Override
	public void insert(T element) {
		
		MyNode<T> node = new MyNode<>(this, ++size);
		node.setValue(element);
		list.add(node);
		for (int i = size / 2; i > 0; i--) {
			heapify(i);
		}
		
	}

	@Override
	public void build(Collection<T> unordered) {
		
		list = new ArrayList<>();
		Iterator<T> ite = unordered.iterator();
		size = 0;
		for (int i = 0; i < unordered.size(); i++) {
			MyNode<T> node = new MyNode<>(this, i + 1);
			node.setValue(ite.next());
			list.add(node);
			size++;
		}
		for (int i = size / 2; i > 0; i--) {
			heapify(i);
		}
		
		
	}

	private void heapify(int i) {
		MyNode<T> c = (MyNode<T>) list.get(i - 1);
		int l = i * 2;
		int r = i * 2 + 1;
		// MyNode<T> l = (MyNode<T>)list.get(i-1).getLeftChild();
		// MyNode<T> r = (MyNode<T>)list.get(i-1).getRightChild();
		int largest = i;
		if (l <= size && list.get(l - 1).getValue().compareTo(c.getValue()) > 0) {
			largest = l;
		}
		if (i * 2 + 1 <= size && list.get(r - 1).getValue().compareTo(list.get(largest - 1).getValue()) > 0) {
			largest = r;
		}
		if (largest != i) {
			MyNode<T> temp = c;
			list.set(i - 1, list.get(largest - 1));
			list.get(i - 1).index = i;
			list.set(largest - 1, temp);
			list.get(largest - 1).index = largest;
			heapify(largest);
		}
	}

	public T extract2() {
		if (size == 0)
			throw null;
		
		
		
		MyNode<T> temp = list.get(0);
		list.set(0, list.get(size - 1));
		list.get(0).index = 1;
		temp.index = size;
		list.set(size-1,temp);
		//list.remove(size - 1);
		//size--;
		if (size != 0)
			heapify(1);
		
	
		return temp.getValue();
		
	}

}
