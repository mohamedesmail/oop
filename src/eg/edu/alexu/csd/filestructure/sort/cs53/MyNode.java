package eg.edu.alexu.csd.filestructure.sort.cs53;

import java.io.BufferedReader;
import java.io.FileReader;

import eg.edu.alexu.csd.filestructure.sort.INode;

public class MyNode<T extends Comparable<T>> implements INode<T> {
	private MyHeap<T> heap;
	int index;
	private T value;
	
	public MyNode(MyHeap<T> h, int i) {
		heap = h;
		index = i;
	}

	@Override
	public INode<T> getLeftChild() {
		
		if(2*index > heap.size){
	
		 return null;
		}
		return heap.list.get(2*index-1);
	}

	@Override
	public INode<T> getRightChild() {
		
		if(2*index+1 > heap.size){
			return null;
		}
		return heap.list.get(2*index+1 -1);
	}

	@Override
	public INode<T> getParent() {
		
		if(index/2 > heap.size){
					return null;
		}
		return heap.list.get(index/2-1);
	}

	@Override
	public T getValue() {
		return value;
	}

	@Override
	public void setValue(T value) {
		this.value = value;
	}
	

		
}
