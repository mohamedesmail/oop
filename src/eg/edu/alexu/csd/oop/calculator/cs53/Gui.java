package eg.edu.alexu.csd.oop.calculator.cs53;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;

public class Gui {

	private JFrame frmCalculator;
	private JTextField textField;
	private JTextField textField_1;
	String equ = new String();
	MyCalculator cal = new MyCalculator();
	boolean isNew = true;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Gui window = new Gui();
					window.frmCalculator.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Gui() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmCalculator = new JFrame();
		frmCalculator.setTitle("Calculator");
		frmCalculator.setBounds(100, 100, 349, 331);
		frmCalculator.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		textField = new JTextField();
		textField.setEditable(false);
		textField.setBounds(10, 11, 213, 34);
		textField.setColumns(10);

		JLabel label = new JLabel("=");
		label.setBounds(233, 13, 14, 27);
		label.setFont(new Font("Tahoma", Font.BOLD, 14));

		textField_1 = new JTextField();
		textField_1.setEditable(false);
		textField_1.setBounds(257, 12, 66, 33);
		textField_1.setColumns(10);
		frmCalculator.getContentPane().setLayout(null);
		frmCalculator.getContentPane().add(textField);
		frmCalculator.getContentPane().add(label);
		frmCalculator.getContentPane().add(textField_1);

		JButton button = new JButton("1");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				equ += "1";
				textField.setText(equ);
				textField_1.setText(new String());
				isNew = true;
			}
		});
		button.setBounds(10, 56, 69, 34);
		frmCalculator.getContentPane().add(button);

		JButton button_1 = new JButton("2");
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				equ += "2";
				textField.setText(equ);
				textField_1.setText(new String());
				isNew = true;
			}
		});
		button_1.setBounds(82, 56, 69, 34);
		frmCalculator.getContentPane().add(button_1);

		JButton button_2 = new JButton("3");
		button_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				equ += "3";
				textField.setText(equ);
				textField_1.setText(new String());
				isNew = true;
			}
		});
		button_2.setBounds(154, 56, 69, 34);
		frmCalculator.getContentPane().add(button_2);

		JButton button_3 = new JButton("/");
		button_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				equ += " / ";
				textField.setText(equ);
				textField_1.setText(new String());
				isNew = true;
			}
		});
		button_3.setBounds(257, 56, 66, 34);
		frmCalculator.getContentPane().add(button_3);

		JButton button_4 = new JButton("4");
		button_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				equ += "4";
				textField.setText(equ);
				textField_1.setText(new String());
				isNew = true;
			}
		});
		button_4.setBounds(10, 90, 69, 34);
		frmCalculator.getContentPane().add(button_4);

		JButton button_5 = new JButton("5");
		button_5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				equ += "5";
				textField.setText(equ);
				textField_1.setText(new String());
				isNew = true;
			}
		});
		button_5.setBounds(82, 90, 69, 34);
		frmCalculator.getContentPane().add(button_5);

		JButton button_6 = new JButton("6");
		button_6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				equ += "6";
				textField.setText(equ);
				textField_1.setText(new String());
				isNew = true;
			}
		});
		button_6.setBounds(154, 90, 69, 34);
		frmCalculator.getContentPane().add(button_6);

		JButton button_7 = new JButton("*");
		button_7.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				equ += " * ";
				textField.setText(equ);
				textField_1.setText(new String());
				isNew = true;
			}
		});
		button_7.setBounds(257, 90, 66, 34);
		frmCalculator.getContentPane().add(button_7);

		JButton button_8 = new JButton("7");
		button_8.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				equ += "7";
				textField.setText(equ);
				textField_1.setText(new String());
				isNew = true;
			}
		});
		button_8.setBounds(10, 124, 69, 34);
		frmCalculator.getContentPane().add(button_8);

		JButton button_9 = new JButton("8");
		button_9.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				equ += "8";
				textField.setText(equ);
				textField_1.setText(new String());
				isNew = true;
			}
		});
		button_9.setBounds(82, 124, 69, 34);
		frmCalculator.getContentPane().add(button_9);

		JButton button_10 = new JButton("9");
		button_10.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				equ += "9";
				textField.setText(equ);
				textField_1.setText(new String());
				isNew = true;
			}
		});
		button_10.setBounds(154, 124, 69, 34);
		frmCalculator.getContentPane().add(button_10);

		JButton button_11 = new JButton("-");
		button_11.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				equ += " - ";
				textField.setText(equ);
				textField_1.setText(new String());
				isNew = true;
			}
		});
		button_11.setBounds(257, 124, 66, 34);
		frmCalculator.getContentPane().add(button_11);

		JButton button_12 = new JButton("+");
		button_12.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				equ += " + ";
				textField.setText(equ);
				textField_1.setText(new String());
				isNew = true;
			}
		});
		button_12.setBounds(257, 163, 66, 34);
		frmCalculator.getContentPane().add(button_12);

		JButton button_13 = new JButton(".");
		button_13.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				equ += ".";
				textField.setText(equ);
				textField_1.setText(new String());
				isNew = true;
			}
		});
		button_13.setBounds(10, 163, 69, 34);
		frmCalculator.getContentPane().add(button_13);

		JButton button_14 = new JButton("0");
		button_14.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				equ += "0";
				textField.setText(equ);
				textField_1.setText(new String());
				isNew = true;
			}
		});
		button_14.setBounds(82, 163, 69, 34);
		frmCalculator.getContentPane().add(button_14);

		JButton button_15 = new JButton("=");
		button_15.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (!equ.equalsIgnoreCase(new String())) {
					if (isNew) {
						cal.input(equ);
					}
					equ = new String();
					String out = cal.getResult();
					textField_1.setText(out);
				}
			}
		});
		button_15.setBounds(154, 163, 69, 34);
		frmCalculator.getContentPane().add(button_15);

		JButton btnPrev = new JButton("previous");
		btnPrev.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String temp = cal.prev();
				if (temp == null) {
					JOptionPane.showMessageDialog(null, "There isn't previous equation");
				} else {
					equ = temp;
					textField.setText(equ);
					textField_1.setText(new String());
					isNew = false;
				}
			}
		});
		btnPrev.setBounds(10, 208, 89, 34);
		frmCalculator.getContentPane().add(btnPrev);

		JButton btnCurrent = new JButton("current");
		btnCurrent.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String temp = cal.current();
				if (temp == null) {
					JOptionPane.showMessageDialog(null, "There isn't current equation");
				} else {
					equ = temp;
					textField.setText(equ);
					textField_1.setText(new String());
					isNew = false;
				}
			}
		});
		btnCurrent.setBounds(122, 208, 89, 34);
		frmCalculator.getContentPane().add(btnCurrent);

		JButton btnNext = new JButton("next");
		btnNext.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String temp = cal.next();
				if (temp == null) {
					JOptionPane.showMessageDialog(null, "There isn't next equation");
				} else {
					equ = temp;
					textField.setText(equ);
					textField_1.setText(new String());
					isNew = false;
				}
			}
		});
		btnNext.setBounds(234, 208, 89, 34);
		frmCalculator.getContentPane().add(btnNext);

		JButton btnSave = new JButton("save");
		btnSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cal.save();
			}
		});
		btnSave.setBounds(10, 245, 89, 36);
		frmCalculator.getContentPane().add(btnSave);

		JButton btnLoad = new JButton("load");
		btnLoad.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cal.load();
			}
		});
		btnLoad.setBounds(122, 246, 89, 34);
		frmCalculator.getContentPane().add(btnLoad);

		JButton btnDelete = new JButton("clear");
		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				equ = new String();
				textField.setText(equ);
				textField_1.setText(new String());
			}
		});
		btnDelete.setBounds(234, 248, 89, 31);
		frmCalculator.getContentPane().add(btnDelete);
	}
}
