package eg.edu.alexu.csd.oop.calculator.cs53;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Formatter;
import java.util.Scanner;

import javax.swing.JOptionPane;

import eg.edu.alexu.csd.oop.calculator.Calculator;

public class MyCalculator implements Calculator {

	Equation currentE = new Equation();
	String[] equations = new String[5];
	int currentIndex = -1;
	int endIndex = -1;
	/* Take input from user */

	public void input(String s) {
		endIndex++;
		if (endIndex > 4) {
			for (int i = 0; i < 4; i++) {
				equations[i] = equations[i + 1];
			}
			equations[4] = s;
			endIndex = 4;
		} else {
			equations[endIndex] = s;
		}
		currentIndex = endIndex;
		this.convert(s);

		/*
		 * String temp = new String(); int i = 0; boolean found = false;
		 * while(!found){ char x = s.charAt(i); if(x != '+' && x != '-' && x !=
		 * '*' && x != '/'){ temp += x; }else{ currentE.operator = x;
		 * currentE.n1 = Double.parseDouble(temp); found = true; temp = new
		 * String(); } i++; } for(--i;i < s.length(); i++){ char x =
		 * s.charAt(i); temp += x; } currentE.n2 = Double.parseDouble(temp);
		 */
	}

	private void convert(String s) {
		/*
		 * if(s.equalsIgnoreCase(new String())){ return; }
		 */
		String temp = new String();
		int i = 0;
		boolean found = false;
		while (!found) {
			char x = s.charAt(i);
			if (x != '+' && x != '-' && x != '*' && x != '/') {
				temp += x;
			} else {
				currentE.operator = x;
				currentE.n1 = Double.parseDouble(temp);
				found = true;
				temp = new String();
			}
			i++;
		}
		for (; i < s.length(); i++) {
			char x = s.charAt(i);
			temp += x;
		}
		currentE.n2 = Double.parseDouble(temp);
	}

	/*
	 * Return the result of the current operations or throws a runtime exception
	 */
	public String getResult() {
		String result = new String();
		double res = 0;
		switch (currentE.operator) {
		case '+':
			res = currentE.n1 + currentE.n2;
			break;
		case '-':
			res = currentE.n1 - currentE.n2;
			break;
		case '*':
			res = currentE.n1 * currentE.n2;
			break;
		case '/':
			res = currentE.n1 / currentE.n2;
			break;
		}
		result = String.valueOf(res);
		return result;
	}

	/* return the current formula */
	public String current() {
		/*
		 * String c = new String(); c += String.valueOf((int)currentE.n1); c +=
		 * currentE.operator; c += String.valueOf((int)currentE.n2);
		 */
		if (currentIndex > 4 || currentIndex < 0) {
			return null;
		}
		this.convert(equations[currentIndex]);
		return equations[currentIndex];
	}

	/*
	 * return the last operation in String format, or Null if no more history
	 * available
	 */
	public String prev() {
		String p = new String();
		int temp = currentIndex - 1;
		if (temp < 0) {
			return null;
		} else {
			currentIndex = temp;
			p = equations[currentIndex];
			this.convert(p);
		}
		return p;
	}

	/*
	 * return the next operation in String format, or Null if no more history
	 * available
	 */
	public String next() {
		String n = new String();
		int temp = currentIndex + 1;
		if (temp > endIndex) {
			return null;
		} else {
			currentIndex = temp;
			n = equations[currentIndex];
			this.convert(n);
		}
		return n;
	}

	/* Save in file the last 5 done Operations */
	public void save() {
		String out = new String();
		for (int i = 0; i < 5; i++) {
			out += equations[i] + "%n";
		}
		out += String.valueOf(currentIndex) + "%n";
		out += String.valueOf(endIndex) + "%n";

		Formatter file;
		try {
			file = new Formatter("results.txt");
			file.format(out);
			file.close();
		} catch (FileNotFoundException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
	}

	/* Load from file the saved operations */
	public void load() {
		Scanner scan = null;

		File file = new File("results.txt");

		try {
			scan = new Scanner(file);
			for (int i = 0; i < 5; i++) {
				equations[i] = scan.nextLine();
			}
			this.currentIndex = Integer.parseInt(scan.nextLine());
			this.endIndex = Integer.parseInt(scan.nextLine());
			scan.close();
		} catch (Exception e1) {
			JOptionPane.showMessageDialog(null, "File not found", "Error", JOptionPane.PLAIN_MESSAGE);
		}

	}
}
